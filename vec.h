#ifndef VEC_H
#define VEC_H

#include <stddef.h>
#include <stdbool.h>

// Library for a growable array with no runtime error checking the user
// doesn't ask for.

// If a vector is valid, the only field that can be 0/NULL is count.
// If invalid, all fields should be 0/NULL.
// It is an error to pass any invalid vector to any of these functions
// except vec_clean.
// Please don't write to this directly.
struct vec {
    void *data;
    size_t elt_size;    // size of 1 elt
    size_t capacity;    // storable elts
    size_t count;       // stored elts
};

// A view into a vector.
// The user can safely construct this themselves.
// src should never be NULL.
struct vec_slice {
    const struct vec *src;
    size_t begin;
    size_t count;
};

// Returns empty vec. On allocation failure, all fields will be set to
// 0/NULL.
// It is an error to have elt_size of zero.
struct vec vec_new(size_t elt_size);

// Creates a new vector, copying elements from a given array.
struct vec vec_from(size_t elt_size, size_t count, const void *src);

// Copies a vector, element by element.
struct vec vec_clone(struct vec src);

// Destroys a vector and returns a pointer to the same data.
// The pointer returned should be cleaned up with free.
// Don't just do `free(v->data);` - you don't know how that data was
// originally allocated.
// Safe to call on NULL or a clean vector - returns NULL in that case.
// Also returns NULL on allocation failure.
void *vec_freeable(struct vec *v);

// Frees all memory used, sets all fields to 0/NULL. Safe to call on
// NULL or on an already clean vector.
void vec_clean(struct vec *);

// Reserves space for new_cap elements in the buffer.
// If new_cap <= count, nothing changes.
// Else, buffer size is set to exactly new_cap.
// Can fail if increasing capacity.
// Returns true on success, false on failure.
bool vec_reserve(struct vec *, size_t new_cap);

// Changes the number of elements in a vector.
// Does not initialise new elements, nor do anything to removed elements.
// Can only fail if increasing number of elements.
// May allocate more space than requested, unlike vec_reserve.
// Returns true on success, false on failure.
bool vec_resize(struct vec *, size_t new_count);

// Returns pointer to the element at index.
// If index is out of bounds, returns NULL.
void *vec_at(struct vec, size_t index);

// Similar to vec_at, but with no bounds checking. If index is out of
// bounds, it simply returns a bad pointer.
void *vec_unsafe_at(struct vec, size_t index);

// Returns pointer to the element at index, but keeps it circular,
// so indices larger than count wrap around to the beginning and indices
// smaller than 0 wrap around to the end.
// Returns NULL if vector has no elements.
void *vec_cat(struct vec, ptrdiff_t index);

// Adds an element to the end.
// If elt != NULL, copies it from elt.
// If elt == NULL, the new element is uninitialised.
// Returns true on success, or false on failure.
// Changes nothing on failure.
bool vec_push(struct vec *, const void *elt);

// Takes off last element, copying it to out if out != NULL.
// Fails and changes nothing if vector is empty.
// Returns true on success, false on failure.
bool vec_pop(struct vec *, void *out);

// Removes last element. It is an error to call this on an empty vector.
// If out != NULL, copies the last element to it.
void vec_unsafe_pop(struct vec *, void *out);

// Do not pass NULL.
struct vec_slice vec_as_slice(const struct vec *v);

// Returns a pointer to an element of a slice. If it's out of range,
// return NULL.
void *vec_slice_at(struct vec_slice s, size_t index);

void *vec_slice_unsafe_at(struct vec_slice s, size_t index);

// Parallel to the circular at function for vectors.
void *vec_slice_cat(struct vec_slice s, ptrdiff_t index);

#endif /* VEC_H */
