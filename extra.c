#include "extra.h"

#include <string.h>
#include <assert.h>

#include "filesys.h"
#include "filesys-internal.h"

ssize_t myfputs(const char *str, MyFILE *stream) {
    assert(str != NULL);
    assert(stream != NULL);
    
    ssize_t written;
    for (written = 0; str[written] != '\0'; written++) {
        if (myfputc(str[written], stream) == EOF) {
            return -1;
        }
    }
    
    return written;
}

ssize_t mycopy(MyFILE *src, MyFILE *dst) {
    assert(src != NULL);
    assert(dst != NULL);
    
    int c;
    ssize_t copied = 0;
    while (c = myfgetc(src), c != EOF) {
        if (myfputc(c, dst) == EOF) {
            copied = -1;
            goto end;
        }
        copied++;
    }
end:
    myfclose(src);
    return copied;
}

ssize_t copy_in(FILE *src, MyFILE *dst) {
    assert(src != NULL);
    assert(dst != NULL);
    
    int c;
    ssize_t copied = 0;
    while (c = fgetc(src), c != EOF) {
        if (myfputc(c, dst) == EOF) {
            copied = -1;
            goto end;
        }
        copied++;
    }
end:
    fclose(src);
    return copied;
}

ssize_t copy_out(MyFILE *src, FILE *dst) {
    assert(src != NULL);
    assert(dst != NULL);
    
    int c;
    ssize_t copied = 0;
    while (c = myfgetc(src), c != EOF) {
        if (fputc(c, dst) == EOF) {
            copied =  -1;
            goto end;
        }
        copied++;
    }
end:
    myfclose(src);
    return copied;
}

void print_fat(void) {
    extern blockno_t fat[NUM_BLOCKS];
    
    // We need to mark blocks as already read without side effects.
    // So we make a copy.
    blockno_t fat_copy[NUM_BLOCKS];
    memcpy(fat_copy, fat, sizeof(fat));
    
    for (blockno_t chain_begin = 1; chain_begin < NUM_BLOCKS; chain_begin++) {
        if (fat_copy[chain_begin] == UNUSED) {
            continue;
        }
        blockno_t chain_next;
        for (
            blockno_t chain_pos = chain_begin;
            chain_pos != END_OF_CHAIN;
            chain_pos = chain_next
        ) {
            assert(chain_pos != UNUSED);
            
            printf("0x%04x -> ", chain_pos);
            
            chain_next = fat_copy[chain_pos];
            fat_copy[chain_pos] = UNUSED;
        }
        printf("X\n");
    }
}
