#ifndef FILESYS_H
#define FILESYS_H

#include <stdbool.h>
#include <stdio.h>

#include <unistd.h>

// Size of each block on the disk.
#define BLOCK_SIZE  (1024)

// Maximum length of a file/directory name (including '\0')
#define MAX_NAME    (256)

typedef struct file_desc MyFILE;


void format(void);
void writedisk(const char *filename);

// Accepted modes: "r", "w".
// Returns NULL if couldn't find/create file or invalid mode.
MyFILE *myfopen(const char *path, const char *mode);
void myfclose(MyFILE *stream);

// Flushes buffer to disk.
// Invalid to call on files in read-mode.
void myfflush(MyFILE *stream);

int myfgetc(MyFILE *stream);

// Returns c normally, EOF if character couldn't be written.
int myfputc(int c, MyFILE *stream);

// Returns true on success, false on failure.
bool mymkdir(const char *path);

// Removes the file at the end of the path. Does not remove directories.
// Returnts true on success, false on failure.
bool myremove(const char *path);

// Removes a directory if it exists, and is empty.
// Returns true if successful, false if not.
bool myrmdir(const char *path);

// Returns true if successful, false if not.
// If unsuccessful, does not change the current directory.
bool mychdir(const char *path);

// Free with free_strlist.
// Returns NULL on failure.
char **mylistdir(const char *path);


#endif // FILESYS_H
