#ifndef UTIL_H
#define UTIL_H

#include <stdint.h>
#include <limits.h>

#include "vec.h"
#include "filesys.h"

#define STATIC_ASSERT(COND,MSG) \
    typedef char static_assertion_##MSG[(COND)?1:-1]

STATIC_ASSERT(CHAR_BIT == 8, crazy_system);

// Unsafe macro - do not use on expressions with side effects.
#define CEIL_DIV(a, b) ((a)/(b) + (a % b != 0))

// Conversion between integers and little-endian bytearrays.
#define int_to_le(x, bytes) _Generic((x),\
    int8_t: i8_to_le,\
    uint8_t: i8_to_le,\
    int16_t: i16_to_le,\
    uint16_t: i16_to_le,\
    int32_t: i32_to_le,\
    uint32_t: i32_to_le,\
    bool: i8_to_le\
)(x, bytes)

#define int_from_le(bytes, x) _Generic((*(x)),\
    int32_t: i32_from_le,\
    uint32_t: i32_from_le,\
    int16_t: i16_from_le,\
    uint16_t: i16_from_le,\
    int8_t: i8_from_le,\
    uint8_t: i8_from_le,\
    bool: i8_from_le\
)(bytes, x)

uint8_t *i8_to_le(int8_t x, uint8_t *bytes);
uint8_t *i16_to_le(int16_t x, uint8_t *bytes);
uint8_t *i32_to_le(int32_t x, uint8_t *bytes);

// Both returns a value, and writes it into `ret`.
// Doesn't write it if `ret` is NULL.
int8_t i8_from_le(const uint8_t *bytes, int8_t *ret);
int16_t i16_from_le(const uint8_t *bytes, int16_t *ret);
int32_t i32_from_le(const uint8_t *bytes, int32_t *ret);


// Returns a NULL-terminated list of strings.
// Clean up with free_strlist.
char **extract_path(const char *path);

// Returns a pointer to the NULL-terminator for `strlist`.
char **strlist_end(char **strlist);

// Prints NULL-terminated list of strings, with `pre` before each one
// and `post` after.
void print_strlist(char *pre, char **list, char *post);

// Frees a NULL-terminated list of strings.
// The strings must be obtained my malloc, as well as the list itself.
void free_strlist(char **strlist);

// Cleans up the strings stored in `v` and the memory owned by `v`
// itself. Does not call free on `v` in any way.
void free_strvec(struct vec *v);

#endif // UTIL_H
