# Virtual Disk

My submission for an assignment in the Operating Systems course at Aberdeen University, 2019.

## The Disk Itself

There are two parts to a virtual disk - "virtual" and "disk". This means that the contents of the virtual disk are held in memory - not an actual disk. It also means that the only way of interfacing with the disk is by reading and writing blocks. This is achieved with the functions `read_raw()` and `write_raw()`. Any other way of reading or writing to the disk **must** be a wrapper for those functions. Having a pointer into the disk is disallowed.

## Nonstandard Libraries

### Posix

I compile with `-std=gnu11`, giving me access to the nonstandard function `strdup()`. This is not strictly necessary - it would be possible to implement myself, but I decided against reinventing the wheel.

The `ssize_t` type is also nice to have.

### Vec

There are a few places in this project where I need a dynamic array. Writing a safe dynamic array in C is a nontrivial task with large amounts of error checking for each insertion. Rather than write all this out repeatedly, I turned to a library I wrote a few months ago in `vec.h` and `vec.c`. As C doesn't have templates/generics, the library is not as efficient as `Vec` from Rust or `std::vector` from C++, and contains far less type information. Nonetheless, it does the job, and is more readable than the alternative. (If you think error-checking distracts from the logic in the current code...)

My personal style of writing C has changed a little bit since then, but I hope the contrast isn't too jarring if you want to take a look inside.

As I didn't want to return a `struct vec`, but rather a `char **`, I had to add the function `vec_freeable`, inspired by `Vec::into_boxed_slice` from Rust.

## Coding Conventions

Keeping a consistent style is important - especially with regards to errors and null pointers. Functions that expect pointers as parameters generally assume nonnull values, unless otherwise stated. The exception is destructors for data types that need to release resources.

I use `assert` liberally for situations that should never happen, and remove them when compiling in release-mode (`-DNDEBUG`). As for errors that I expect to happen, I try to detect as quickly as possible and propogate the error upwards. You'll see variations of this idiom cropping up a lot in my code.

```
if (function_that_can_fail() == error_value1) {
    return_value = error_value2;
    goto end;
}

...

end:
    free_resources();
    return return_value;
```

## Implementation Explanation

### Data Structures

There are three kinds of blocks stored on disk - FAT blocks, directory blocks, and data blocks.

+ **Data blocks** have exactly the same representation on disk and in memory, byte-for-byte.

+ **FAT blocks** store an array of 16-bit integers. These integers have whatever representation they have in memory, but they are always stored in little-endian format on the disk.

+ **Directory blocks** have a small array of entries. Since they are read and written so commonly, I found it useful to package this information with their address on the disk. Of course, that doesn't need to be written to disk; it would be like having an array of integers where `array[i] == i` for all `i` - you're just storing information you already know.

+ **Directory entries** tell you how to read a file. If the field `in_use` is `false`, it holds no file and can be safely overridden to create a new file. If it does hold a file, it stores the file's name, what kind of file it is (directory, data, or FAT), how large the file is (unless the file is a directory) and the address of the file's first block.

+ **File descriptors** are how the user of this library interacts with files. They store temporary information, like the current position in the file, and also permanent information about the file itself, like its size.

### Disk Structrue

+ Block 0 contains just the name of the disk. Blocks 1 & 2 contain the file-allocation table. If the disk were larger, all the blocks of the FAT would still need to be contiguous. The root directory begins at block 3. In addition to knowing its parent, the root directory also knows about the FAT.

+ All directories contain an entry ".." (`2e 2e 00` in hex) for their parent directory. In the special case of the root directory, root is its own parent. At the conceptual level, directories have an entry "." for themselves, but this information is not actually stored in their entry lists. Instead, they search for themselves in their parent directory.
  
  While every directory has a ".." in its entry list, not every directory *block* does. A single directory can span several blocks. They are chained using the FAT just like data files, and the FAT itself.

+ Root's name is `""`, the empty string. This just turned out to be the most natural thing when parsing.

+ Control information (anything that's not in a data block) is packed and stored in little-endian format.

### Global Variables

I have a few variables with static lifetime. Understanding them all is important.

+ **`virtual_disk`** simulates a hard disk, through the `read_raw`() and `write_raw()` functions.

+ **`fat`** is the in-memory cache of the file-allocation-table. It has to be written to the disk every now and then with `sync_fat`.

+ **`current_dir_no`** is the block-address of the beginning of the current directory. For example, if you're in root, it will be set to 3. `read_dir(current_dir_no, ...)` comes up a lot.

### Functions

In these explanations, I'm omitting explanations of error-checking, unless there's something interesting to talk about. It distracts enough from the C code - I don't need it distracting from the explanations too. The same goes for freeing resources.

#### `format()`

First, we zero out every byte of the virtual disk. This is done by taking a blank block of memory and copying it to every block on the disk.

After that, we write the name of the disk to a temporary memory block which we copy to block 0.

Then we initialise the root. This means blanking it out, adding ".." and "fat" as entries and writing it to the disk. Additionally, we set the global variable `current_block_no` to the address of the root.

Initialising the FAT is next. Block 0 is unavailable. Block 1 is the beginning of the FAT, followed by block 2, which ends the FAT. Block 3 begins and ends the root. Everything else if free to use.

Finally, we synchronise the FAT, which consists of breaking the FAT into segments and writing each segment to disk.

#### `myfopen()`

When we parse the the filepath, the last string in the extracted list is the name of the file we're interested in. We use `follow_path_m1()` to follow the path down without trying to open the file as if it were a directory. Since we're changing directories, we need to save `current_dir_no` so we can restore it and the caller doesn't know anything has changed.

We allocate space for the file descriptor and work out whether we're opening it in read- write- or append-mode. (Append is unimplemented.)

+ If we're reading, we check whether we have found the file. If not, it can't be read and we exit. If it has, we fill the file descriptor's buffer and initialise it to read from the beginning.

+ If we're writing, we also check whether or not we have found the file, though we do something else with the information. If the file exists, check that it's safe to truncate before doing so. If the file doesn't exist, we create it from scratch (`create_file()` searches through a directory for a free slot, and extends it if none are found.) We initialise the file descriptor to start writing at the beginning, and set the size of the file to zero.

If a directory begins in block `a`, extends to block `b`, which contains the entry for a file beginning at block `c`, the file descriptor told about `b` and `c`.

#### `myfclose()`

If the file to close is in read-mode, this is laughably simple. There's nothing that needs updating so we can just deallocate the file descriptor.

If the file is in write-mode, we must first write out all the changes that have occurred since we last flushed.

#### `myfputc()`

I use buffered file I/O with a buffer-size of one block. Normally we can just write one character to the buffer and update our position. But if the buffer is full, we have to flush it out to the disk before resetting our position in the buffer. Then we can write to the buffer like normal.

Note that the file size begins at 0 and increases by 1 with every character successfully written.

#### `myfgetc()`

The buffering logic is extremely similar to that of `myfputc()`, except that we're reading instead of writing. Instead of zeroing out the buffer when we're done with it, we read the next one from the disk.

First though, we have to check whether the file is done or not. Nothing too complicated.

#### `myfflush()`

There are two things this function must do. It must write the buffer out to disk and it must update the file size in the directory that owns relevant file.

The `dir` field of `struct file_desc` stores the block number of the directory block in which the file is listed, so finding the file's home is a linear search with bounded search-space.

Writing the buffer to disk is also simple - just call the `write_raw`function.

As these two tasks are independent, one could conceivably use multiple threads. I don't know if the overhead from multithreading relatively these short tasks outweighs the benefits from parallelism.

**`mychdir()`**

This function is incredibly integral to the rest of the library. It consists of two parts - parsing the path into a list of directories that can be entered one after the other, and actually following that path. Since these also had to be done elsewhere, I created seperate functions for them.

**`extract_path`** (Located in util.c)

First, let's talk about the intended behaviour of this function. Given the string `"path/to//directory/./"` it should return `{"", "path", "to", "", "directory", "."}`. The size of the returned array can't be known in advance, and the strings it holds should be duplicates owned by the array - not pointers into the original string.

This is the first place I use `struct vec`. We have a `struct vec` called `parts` that stores each directory in the sequence. The code looks complicated, but the algorithm is simple.

```
while (we haven't read the whole string) {
    read the name of one file/directory
    mark the new position in the string to read from
    add the new name to the dynamic array
}
```

A `struct vec` knows its size, but it has almost no type information, and absolutely no type information known at compile-time. While it's great for building dynamic arrays, it's a poor choice for the return type. `char **` is a great choice for the return type - it's an array of strings! Unfortunately, a `char **` does not know how many elements it has, so it needs a `NULL` to mark the end.

#### `follow_path()`

This function expects an array of strings (such as the kind returned by `extract_path`) and moves the current directory from one end to the other. Generally, this is pretty simple: read in the current directory - search for the next one in its entries and move to it. This works even for the parent directory `".."`. Because of some special logic in `search_dir_by_name()`, even `"."` and `""` behave nicely.

I've taken care to change nothing if the path cannot be followed, by only chaging `current_dir_no` just before returning successfully.

**`search_dir_by_name()`**

This function by itself is nothing special, but it's a simple example of a pattern I found incredibly useful in this project. I'm quite proud of it. Here's the problem:

> You have a directory and the name of a file; you want the entry in the directory that holds that file. You may want to mutate the entry, so you want a pointer to the entry itself, not a copy.
> 
> Directories can span multiple blocks, and the entry you're looking for could be in any one of them. You only have one directory block, and it might not have the entry you want. How do you get the pointer?

This would be simple if we could have a pointer into the virtual disk, but as mentioned, the virtual disk is a virtual **disk** - it can only be interfaced with `read_raw()` and `write_raw()`.

There are various ways this problem could be solved; most involve returning some kind of dynamically-allocated memory, which is just asking for trouble. I try to avoid that wherever possible. The solution I came up with was to mutate the directory block itself so that when the function returns, the directory block holds the entry that the return value points at.

This pattern is also used in `search_dir_by_begin()`, `create_file()`, `add_entry()`, and `find_free_dir_entry()`

The algorithm is just a linear search with two special cases: searching for self and root. Root is simple - we overwrite the directory block with the first block of the root directory, and search for its parent. We can do this because we know that root is its own parent.

Searching for the current directory is trickier. Say we're in directory `A`. We note down the block at which `A` begins and move to `A/..`. Then we search the current directory (`A/..`) for a file that begins at the same block as `A`. As we never allocate the same block to two different files, this gives us the entry that stores `A`. When we're done, we have to reset `current_dir_no`.

**`mymkdir()`**

This function takes a path of directories, and creates as many of them as necessary so that the path leads to a valid directory.

We start by extracting the path, as normal for functions taking a path parameter. We change directories as we traverse the path, so we save `current_dir_no` to restore it afterwards.

We step through each directory in the path one at a time. At each step, we search for the directory we want to move to in the current directory. If it exists, we simply move to it. If it doesn't, we have to create a new directory, name it, and let it know about its parent. Once all of that is done without error, we can move to it.

**`mylistdir()`**

This function builds and returns a dynamic array of strings the same way as `extract__path()`, so I won't go into detail again about how that works.

We start by changing directory into the path we want to list from. We need to save the value of `current_dir_no` so the caller doesn't notice this.

Then we go through all the blocks in the current directory in turn. For each one, we record the names of each file currently allocated, and then look up the FAT to move to the next directory block.

**`myremove()`**

As always, we're changing directory in this function, so we have to save `current_dir_no` and restore it.

We begin by extracting the path of the file to remove. The file is the last string in the list, so we use `follow_path_m1()` instead of `follow_path()`. We search the current directory for a file of the appropriate name. If it's found, we let the FAT know that all blocks used by that file are `UNUSED` and set the `in_use` field in the directory entry to `false`, so the space can be reused, and so the file doesn't appear in searches.

When we're done with all of that, we write out the current directory block to disk.

**`myrmdir()`**

This is almost exactly the same as `myremove()`. There is slightly more error checking - we don't want to remove a directory that actually has files, the current directory, or the root directory.

### Known Issues

+ Don't try to put forward-slashes into file names. That includes directories.

## What I Should Have Done Differently

+ The current code does too much monkeying around with `current_dir_no`. There has to be a lot of saving and restoring. Instead of changing `current_dir_no`, `follow_path()` should have changed an arbitrary `blockno_t` that was passed to it by pointer.

+ Functions that can fail can often fail in several different ways. To list a few:
  
  + You can try to open a file that doesn't exist.
  
  + You can request memory from `malloc()` and fail to get it.
  
  + You can request a free block on the disk and fail to get it.
  
  The current code does check for these and report that something went wrong. It would be better if it could say *what* went wrong. One way of doing this would be to have a global variable `filesys_errno` and set it to a predefined constant for everything that can go wrong (`FILESYS_OK`, `FILESYS_MEM_ALLOC_FAIL`, `FILESYS_FILE_NOT_FOUND`, etc)

+ Conversion between packed little-endian integers in the disk and 'whatever my machine does' integers is not as clean as I would like. I wrote the conversion functions manually, violating DRY. I know some C programmers hate complex macros, especially for code-generation, but I think they would be appropriate here. 
  
  Because I wrote the functions manually, I didn't want to go through the trouble of writing both signed and unsigned versions. So when I called them in `unpack_dir_entry` I got warnings from `gcc` about signedness of pointer targets. When I get compiler warnings, I either fix my code or disable the warning. I never consider 'compiles with warnings' good enough. Signedness is important, so I suppressed the warning by adding explicit casts. This defeats the point of `typedef`ing and using `_Generic`.
