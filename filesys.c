#include "filesys.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "filesys-internal.h"

static blockno_t current_dir_no;

static void clear_disk(void);
static void write_control_info(void);
static void init_root(void);
static void init_fat(void);

// Searches a directory for a file of a given name.
// Returns a pointer to the relevant entry, or NULL if not found.
// Modifies `dir` so that the return value points into it.
// Does not process file paths at all. Don't use this to search for
// something that might be in a subdirectory.
static struct dir_entry *search_dir_by_name(
    struct dir_block *dir,
    const char *name
);
static struct dir_entry *search_dir_by_begin(
    struct dir_block *dir,
    blockno_t begin
);

// mychdir is basically a wrapper for this.
// Expects a NULL-terminated list of strings.
static bool follow_path(char **path);

// "follow path minus one"
// Follows a path to the second-last entry.
static bool follow_path_m1(char **path);

// Creates a new file in  a directory.
// Returns a pointer to the relevant entry in the directory, of NULL
// if the file could not be created.
// Modifies `dir` so the return value points into it.
// Undefined behaviour if a file of the same name already exists in the
// directory.
static struct dir_entry *create_file(
    struct dir_block *dir,
    block_kind_t kind,
    const char *name
);

// Adds a new entry in a directory, extending the directory if needed
// (and modifying `dir`).
// Returns a pointer to the new entry in the directory, NULL if it
// it couldn't be created.
static struct dir_entry *add_entry(
    struct dir_block *restrict dir,
    struct dir_entry *restrict entry
);

// Returns a pointer to a free dir_entry in `dir`. If there is no free
// entry, returns NULL.
// Modifies `dir` so that the return value points into it.
static struct dir_entry *find_free_dir_entry(struct dir_block *dir);

// Returns the blockno of a free block if there is one, negative if the
// disk is full. Has no side-effects.
static blockno_t find_free_block(void);

// Allocates a new block for a file and links it to the old end in the
// FAT.
// Returns the block number of the new block, or negative if there was
// no space.
static blockno_t extend_file(blockno_t end);

// Marks each block following `start` as unused.
static void free_block_chain(blockno_t start);


void format(void) {
    extern blockno_t current_dir_no;
    
    clear_disk();
    write_control_info();
    init_root();
    current_dir_no = ROOT_BEGIN;
    init_fat();
    sync_fat();
}

MyFILE *myfopen(const char *path, const char *mode) {
    assert(path != NULL);
    assert(mode != NULL);
    
    extern blockno_t fat[NUM_BLOCKS];
    extern blockno_t current_dir_no;
    
    MyFILE *file = NULL;
    
    extern blockno_t current_dir_no;
    blockno_t saved = current_dir_no;
    
    // Parse the path.
    char **extracted = extract_path(path);
    if (extracted == NULL) {
        return NULL;
    }
    
    // Go to the relevant directory.
    if (follow_path_m1(extracted) == false) {
        goto end;
    }
    char *filename = strlist_end(extracted)[-1];
    
    struct dir_block current_dir;
    read_dir(current_dir_no, &current_dir);
    struct dir_entry *location = search_dir_by_name(&current_dir, filename);
    
    file = malloc(sizeof(MyFILE));
    if (file == NULL) {
        goto end;
    }
    
    file->mode = parse_mode(mode);
    if (file->mode < 0) {
        free(file);
        file = NULL;
        goto end;
    }
    
    switch (file->mode) {
        case READ:
            if (location == NULL) {
                free(file);
                file = NULL;
                goto end;
            }
            *file = (struct file_desc) {
                .mode = READ,
                .begin = location->begin,
                .block_no = location->begin,
                .offset = 0,
                .pos = 0,
                .size = location->size,
                .dir = current_dir.self_addr,
            };
            read_raw(file->begin, &file->buf);
            break;
        
        case WRITE:
            if (location != NULL) {
                // A file of the same name already existed.
                if (location->kind != RAW) {
                    // It's not a good idea to overwrite directories or
                    // FAT files.
                    free(file);
                    file = NULL;
                    goto end;
                }
                // Free all but the first block.
                free_block_chain(fat[location->begin]);
            }
            else {
                // The file did not already exist.
                // Reread the directory (it was changed when we searched
                // for the file) and create a new entry for the file.
                read_dir(current_dir_no, &current_dir);
                location = create_file(&current_dir, RAW, filename);
                if (location == NULL) {
                    free(file);
                    file = NULL;
                    goto end;
                }
            }
            *file = (struct file_desc) {
                .mode = WRITE,
                .begin = location->begin,
                .block_no = location->begin,
                .offset = 0,
                .pos = 0,
                .size = location->size = 0,
                .dir = current_dir.self_addr
            };
            memset(&file->buf, 0, BLOCK_SIZE);
            break;
        
        case APPEND: // TODO
            fprintf(stderr, "append mode unimplemted.\n");
            abort();
            // Find file in dir, create if not found.
            // Forward to end.
            break;
        
        default:
            free(file);
            file = NULL;
            goto end;
    }
    
end:
    current_dir_no = saved;
    free_strlist(extracted);
    return file;
}

void myfclose(MyFILE *stream) {
    if (stream == NULL) {
        return;
    }
    if (stream->mode != READ) {
        myfflush(stream);
    }
    
    free(stream);
}

void myfflush(MyFILE *stream) {
    assert(stream != NULL);
    assert(stream->mode != READ);
    
    // Find own entry in parent directory, to update size.
    struct dir_block dir;
    read_dir(stream->dir, &dir);
    
    struct dir_entry *location = search_dir_by_begin(&dir, stream->begin);
    assert(location != NULL && "Could not find entry to flush to");
    
    // Update size.
    location->size = stream->size;
    write_dir(&dir);
    
    // Write current buffer to disk.
    write_raw(stream->block_no, &stream->buf);
}

int myfgetc(MyFILE *stream) {
    assert(stream != NULL);
    assert(stream->mode == READ);
    assert(0 <= stream->offset);
    assert(stream->offset <= BLOCK_SIZE);
    assert(stream->pos <= stream->size);
    
    extern blockno_t fat[NUM_BLOCKS];
    
    if (stream->pos == stream->size) {
        return EOF;
    }
    
    if (stream->offset == BLOCK_SIZE) {
        // We've read everything from the buffer. Get the next one.
        
        blockno_t next_block = fat[stream->block_no];
        // We know we're not finished becuase pos < size.
        assert(next_block != END_OF_CHAIN);
        assert(next_block != UNUSED);
        
        stream->block_no = next_block;
        stream->offset = 0;
        read_raw(stream->block_no, &stream->buf);
    }
    
    int ret = stream->buf.data[stream->offset];
    
    stream->offset++;
    stream->pos++;
    
    return ret;
}

int myfputc(int c, MyFILE *stream) {
    assert(stream != NULL);
    assert(stream->mode != READ);
    assert(0 <= stream->offset);
    assert(stream->offset <= BLOCK_SIZE);
    
    if (stream->offset == BLOCK_SIZE) {
        // We've filled our buffer.
        myfflush(stream);
        
        // Get a new block to write to.
        blockno_t new_block = extend_file(stream->block_no);
        if (new_block < 0) {
            return EOF;
        }
        
        stream->block_no = new_block;
        stream->offset = 0;
        memset(&stream->buf, 0, BLOCK_SIZE);
    }
    
    // Write to the buffer.
    stream->buf.data[stream->offset] = (uint8_t) c;
    stream->offset++;
    stream->pos++;
    stream->size++;

    return c;
}

bool mymkdir(const char *path) {
    assert(path != NULL);
    
    extern blockno_t current_dir_no;
    blockno_t saved = current_dir_no;
    
    bool ok = true; // Whether we encountered error or not.
    
    char **extracted = extract_path(path);
    if (extracted == NULL) {
        ok = false;
        goto end;
    }
    
    for (size_t i = 0; extracted[i] != NULL; i++) {
        // Search for the next directory in the sequence.
        struct dir_block current_dir;
        read_dir(current_dir_no, &current_dir);
        struct dir_entry *next = search_dir_by_name(
            &current_dir,
            extracted[i]
        );
        if (next == NULL) {
            // The directory didn't exist. We have to make it.
            next = create_file(&current_dir, DIR, extracted[i]);
            if (next == NULL) {
                // We couldn't create a new directory.
                ok = false;
                goto end;
            }
            // Initialise the new directory.
            struct dir_block new_dir;
            blank_dir(next->begin, &new_dir);
            
            // Let the new directory know who its parent is.
            struct dir_entry parent = {
                .begin = current_dir_no,
                .kind = DIR,
                .size = 0, // We don't really use size for directories.
                .in_use = true,
            };
            memset(parent.name, 0, MAX_NAME);
            strcpy(parent.name, "..");
            if (add_entry(&new_dir, &parent) == NULL) {
                ok = false;
                goto end;
            }
            write_dir(&new_dir);
        }
        else if (next->kind != DIR) {
            // There is something by that name, but it's not a directory.
            ok = false;
            goto end;
        }
        
        // Move to it.
        if (mychdir(extracted[i]) == false) {
            ok = false;
            goto end;
        };
    }
    
end:
    free_strlist(extracted);
    current_dir_no = saved;
    sync_fat();
    return ok;
}

bool myremove(const char *path) {
    assert(path != NULL);
    
    extern blockno_t current_dir_no;
    blockno_t saved = current_dir_no;
    
    bool ok = true;
    
    char **extracted = extract_path(path);
    if (extracted == NULL) {
        ok = false;
        goto end;
    }
    if (extracted[0] == NULL) {
        // Blank filepath.
        ok = false;
        goto end;
    }
    
    // Go to the directory the file lives in.
    if (follow_path_m1(extracted) == false) {
        ok = false;
        goto end;
    }
    
    // Look for the entry.
    struct dir_block current_dir;
    read_dir(current_dir_no, &current_dir);
    char *filename = strlist_end(extracted)[-1];
    
    struct dir_entry *location = search_dir_by_name(
        &current_dir,
        filename
    );
    if (location == NULL) {
        // The file doesn't exist.
        ok = false;
        goto end;
    }
    if (location->kind != RAW) {
        // We don't want to remove anything that's not a data file.
        ok = false;
        goto end;
    }
    
    // Remove the file.
    free_block_chain(location->begin);
    location->in_use = false;
    
    // Write to disk.
    write_dir(&current_dir);

end:
    current_dir_no = saved;
    free_strlist(extracted);
    return ok;
}

bool myrmdir(const char *path) {
    assert(path != NULL);
    
    extern blockno_t current_dir_no;
    blockno_t saved = current_dir_no;
    
    bool ok = true;
    
    char **entry_names = NULL; // Comes into play later.
    
    char **extracted = extract_path(path);
    if (extracted == NULL) {
        ok = false;
        goto end;
    }
    if (extracted[0] == NULL) {
        // Blank filepath.
        ok = false;
        goto end;
    }
    
    // Go to the directory the file lives in.
    if (follow_path_m1(extracted) == false) {
        ok = false;
        goto end;
    }
    
    // Look for the entry.
    struct dir_block current_dir;
    read_dir(current_dir_no, &current_dir);
    char *dirname = strlist_end(extracted)[-1];
    
    struct dir_entry *location = search_dir_by_name(
        &current_dir,
        dirname
    );
    if (location->begin == ROOT_BEGIN) {
        // Removing root is a BAD idea.
        ok = false;
        goto end;
    }
    else if (location->begin == saved) {
        // Removing the current directory is also a bad idea.
        ok = false;
        goto end;
    }
    
    if (location == NULL) {
        // The directory doesn't exist.
        ok = false;
        goto end;
    }
    if (location->kind != DIR) {
        // We don't want to remove anything that's not a directory.
        ok = false;
        goto end;
    }
    
    // Check that the directory is empty before removing it.
    entry_names = mylistdir(dirname);
    if (entry_names == NULL) {
        ok = false;
        goto end;
    }
    
    char **terminator = strlist_end(entry_names);
    
    // Empty directory <==> entry_names == {"..", ".", NULL}
    bool empty = terminator - entry_names == 2;
    if (!empty) {
        ok = false;
        goto end;
    }
    
    // Remove the directory.
    free_block_chain(location->begin);
    location->in_use = false;
    
    // Write to disk.
    write_dir(&current_dir);

end:
    current_dir_no = saved;
    free_strlist(extracted);
    free_strlist(entry_names);
    return ok;
}

bool mychdir(const char *path) {
    assert(path != NULL);
    
    char **extracted = extract_path(path);
    if (extracted == NULL) {
        return false;
    }
    
    bool ok = follow_path(extracted);
    free_strlist(extracted);
    return ok;
}

char **mylistdir(const char *path) {
    assert(path != NULL);
    
    extern blockno_t fat[NUM_BLOCKS];
    
    extern blockno_t current_dir_no;
    blockno_t saved = current_dir_no;
    
    if (mychdir(path) == false) {
        return false;
    }
    
    struct vec list = vec_new(sizeof(char *));
    if (list.data == NULL) {
        goto err;
    }
    
    // "." isn't stored as an entry, but we want it anyway.
    char *self = strdup(".");
    if (self == NULL) {
        goto err;
    }
    if (vec_push(&list, &self) == false) {
        goto err;
    }
    
    while (true) {
        struct dir_block dir;
        read_dir(current_dir_no, &dir);
        // Read from the current directory block.
        for (size_t i = 0; i < ENTRIES_PER_DIR_BLOCK; i++) {
            if (dir.entries[i].in_use) {
                char *filename = strdup(dir.entries[i].name);
                if (filename == NULL) {
                    goto err;
                }
                if (vec_push(&list, &filename) == false) {
                    goto err;
                }
            }
        }
        blockno_t next = fat[current_dir_no];
        assert(next != UNUSED);
        if (next != END_OF_CHAIN) {
            // Go to the next block for this directory.
            current_dir_no = next;
        }
        else {
            // The directory is over.
            break;
        }
    }
    
    current_dir_no = saved;
    
    char *terminator = NULL; // Hasta La Vista, baby.
    if (vec_push(&list, &terminator) == false) {
        goto err;
    }
    
    return vec_freeable(&list);
    
err:
    current_dir_no = saved;
    free_strvec(&list);
    return NULL;
}


static void clear_disk(void) {
    struct raw_block raw;
    memset(&raw, 0, sizeof(raw));
    for (blockno_t i = 0; i < NUM_BLOCKS; i++) {
        write_raw(i, &raw);
    }
}

static void write_control_info(void) {
    struct raw_block info;
    memset(&info, 0, sizeof(struct raw_block));
    strcpy((char *) info.data, "CS3026 Operating Systems Assessment");
    write_raw(CONTROL_BEGIN, &info);
}

static void init_root(void) {
    struct dir_block root;
    
    blank_dir(ROOT_BEGIN, &root);
    
    // For reference: https://www.youtube.com/watch?v=eYlJH81dSiw
    struct dir_entry parent = {
        .begin = ROOT_BEGIN,
        .kind = DIR,
        .size = 0,
        .in_use = true,
        .name = ".."
    };
    
    struct dir_entry fat_source = {
        .begin = FAT_BEGIN,
        .kind = FAT,
        .size = NUM_BLOCKS * sizeof(blockno_t),
        .in_use = true,
        .name = "fat"
    };
    
    root.entries[0] = parent;
    root.entries[1] = fat_source;
    
    write_dir(&root);
}

static void init_fat(void) {
    extern blockno_t fat[NUM_BLOCKS];
    
    // Control block.
    fat[0] = END_OF_CHAIN;
    
    // Fat blocks.
    for (blockno_t i = 0; (unsigned) i < FAT_BLOCKS - 1; i++) {
        fat[FAT_BEGIN + i] = FAT_BEGIN + i + 1;
    }
    fat[FAT_BEGIN + FAT_BLOCKS - 1] = END_OF_CHAIN;
    
    // Root.
    fat[ROOT_BEGIN] = END_OF_CHAIN;
    
    // Everything else.
    for (blockno_t i = ROOT_BEGIN + 1; (unsigned) i < NUM_BLOCKS; i++) {
        fat[i] = UNUSED;
    }
}

static struct dir_entry *search_dir_by_name(
    struct dir_block *dir,
    const char *name
) {
    assert(dir != NULL);
    assert(name != NULL);
    
    // Special case: self.
    if (strcmp(".", name) == 0) {
        // Find own entry in parent directory.
        extern blockno_t current_dir_no;
        blockno_t saved = current_dir_no;
        static struct dir_entry *ret;
        
        blockno_t search_for = dir->self_addr;
        current_dir_no = dir->self_addr;
        if (mychdir("..") == false) {
            ret = NULL;
        }
        else {
            struct dir_block parent;
            read_dir(current_dir_no, &parent);
            ret = search_dir_by_begin(&parent, search_for);
        }
        current_dir_no = saved;
        return ret;
    }
    
    // Special case: root.
    if (strcmp("", name) == 0) {
        read_dir(ROOT_BEGIN, dir);
        name = "..";
    }
    
    // General case.
    extern blockno_t fat[NUM_BLOCKS];
    
    while (true) {
        // Search for the file in the current dir_block.
        for (size_t i = 0; i < ENTRIES_PER_DIR_BLOCK; i++) {
            if (
                dir->entries[i].in_use && 
                strncmp(name, dir->entries[i].name, MAX_NAME) == 0
            ) {
                return dir->entries + i;
            }
        }
        
        // If not found, move to the next dir_block in the directory.
        blockno_t next = fat[dir->self_addr];
        assert(next != UNUSED);
        if (next != END_OF_CHAIN) {
            // There's more to this directory.
            read_dir(next, dir);
        }
        else {
            // End of the line.
            return NULL;
        }
    }
}

// Basically the same algorithm, but with integers instead of strings.
static struct dir_entry *search_dir_by_begin(
    struct dir_block *dir,
    blockno_t begin
) {
    assert(dir != NULL);
    
    extern blockno_t fat[NUM_BLOCKS];
    
    while (true) {
        for (size_t i = 0; i < ENTRIES_PER_DIR_BLOCK; i++) {
            if (dir->entries[i].in_use && dir->entries[i].begin == begin) {
                return dir->entries + i;
            }
        }
        
        blockno_t next = fat[dir->self_addr];
        assert(next != UNUSED);
        if (next != END_OF_CHAIN) {
            read_dir(next, dir);
        }
        else {
            return NULL;
        }
    }
}

static bool follow_path(char **path) {
    assert(path != NULL);
    
    extern blockno_t current_dir_no;
    // Don't want to corrupt current_dir_no if we fail.
    blockno_t temp_dir_no = current_dir_no;
    
    for (size_t i = 0; path[i] != NULL; i++) {
        // Search for the next directory in the current one.
        struct dir_block current_dir;
        read_dir(temp_dir_no, &current_dir);
        struct dir_entry *next_entry = search_dir_by_name(
            &current_dir,
            path[i]
        );
        if (next_entry == NULL || next_entry->kind != DIR) {
            // Directory does not exist.
            return false;
        }
        // Move to it.
        temp_dir_no = next_entry->begin;
    }
    
    current_dir_no = temp_dir_no;
    
    return true;
}

static bool follow_path_m1(char **path) {
    assert(path != NULL);
    
    extern blockno_t current_dir_no;
    blockno_t saved = current_dir_no;
    
    char **terminator = strlist_end(path);
    
    if (terminator == path) {
        // The path is empty. Not even "."
        goto err;
    }
    
    char *last = terminator[-1];
    terminator[-1] = NULL;
    
    bool ok = follow_path(path);
    
    terminator[-1] = last;
    
    return ok;
    
err:
    current_dir_no = saved;
    return false;
}

static struct dir_entry *create_file(
    struct dir_block *dir,
    block_kind_t kind,
    const char *name
) {
    assert(dir != NULL);
    assert(name != NULL);
    
    struct dir_entry temp_entry = {
        .begin = find_free_block(),
        .kind = kind,
        .size = 0,
        .in_use = true,
    };
    memset(temp_entry.name, 0, sizeof(temp_entry.name));
    strncpy(temp_entry.name, name, MAX_NAME - 1);
    
    if (temp_entry.begin < 0) {
        // There was no room for a new file.
        return NULL;
    }
    fat[temp_entry.begin] = END_OF_CHAIN;
    
    struct dir_entry *ret = add_entry(dir, &temp_entry);
    if (ret == NULL) {
        // We couldn't add the entry to the directory.
        fat[temp_entry.begin] = UNUSED; // Reclaim block.
        return NULL;
    }
    
    sync_fat();
    return ret;
}

static struct dir_entry *add_entry(
    struct dir_block *restrict dir,
    struct dir_entry *restrict entry
) {
    assert(dir != NULL);
    assert(entry != NULL);
    
    extern blockno_t fat[NUM_BLOCKS];
    
    struct dir_entry *free_location = find_free_dir_entry(dir);
    if (free_location == NULL) {
        // No room in the directory. We make more room.
        
        // find_free_dir_entry should move us to the last block of this
        // directory.
        assert(fat[dir->self_addr] == END_OF_CHAIN);
        
        blockno_t next_block = extend_file(dir->self_addr);
        if (next_block < 0) {
            // We couldn't make more room.
            return NULL;
        }
        
        blank_dir(next_block, dir);
        free_location = dir->entries;
    }
    
    *free_location = *entry;
    write_dir(dir);
    
    return free_location;
}

static struct dir_entry *find_free_dir_entry(struct dir_block *dir) {
    assert(dir != NULL);
    
    extern blockno_t fat[NUM_BLOCKS];
    
    while (true) {
        // Search the current dir_block.
        for (size_t i = 0; i < ENTRIES_PER_DIR_BLOCK; i++) {
            if (!dir->entries[i].in_use) {
                return dir->entries + i;
            }
        }
        // If not found, move onto the next one.
        blockno_t next = fat[dir->self_addr];
        assert(next != UNUSED);
        if (next != END_OF_CHAIN) {
            read_dir(next, dir);
        }
        else {
            return NULL;
        }
    }
}

static blockno_t find_free_block(void) {
    extern blockno_t fat[NUM_BLOCKS];
    
    blockno_t start = 0;
    //blockno_t start = rand();
    for (blockno_t i = 0; i < NUM_BLOCKS; i++) {
        blockno_t block_no = (start + i) % NUM_BLOCKS;
        if (fat[block_no] == UNUSED) {
            return block_no;
        }
    }
    
    return -1;
}

static blockno_t extend_file(blockno_t end) {
    extern blockno_t fat[NUM_BLOCKS];
    
    assert(fat[end] == END_OF_CHAIN);
    
    blockno_t new_end = find_free_block();
    if (new_end < 0) {
        return -1;
    }
    
    fat[end] = new_end;
    fat[new_end] = END_OF_CHAIN;
    
    sync_fat();
    return new_end;
}

static void free_block_chain(blockno_t start) {
    extern blockno_t fat[NUM_BLOCKS];
    
    blockno_t next;
    for (blockno_t curr = start; curr != END_OF_CHAIN; curr = next) {
        assert(curr != UNUSED);
        
        next = fat[curr];
        fat[curr] = UNUSED;
    }
    
    sync_fat();
}
