#ifndef FILESYS_INTERNAL_H
#define FILESYS_INTERNAL_H


#include "filesys.h"

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "util.h"


#define NUM_BLOCKS              (1024)

#define ENTRIES_PER_FAT_BLOCK   (BLOCK_SIZE / sizeof(blockno_t))
#define FAT_BLOCKS              (CEIL_DIV(NUM_BLOCKS,ENTRIES_PER_FAT_BLOCK))

#define ENTRIES_PER_DIR_BLOCK   ((BLOCK_SIZE - sizeof(blockno_t)) / sizeof(struct dir_entry))

#define CONTROL_BEGIN           (0)
#define FAT_BEGIN               (CONTROL_BEGIN + 1)
#define ROOT_BEGIN              (FAT_BEGIN + FAT_BLOCKS)


// Identifies a block within the virtual disk.
typedef int16_t blockno_t;
#define UNUSED          (-1)
#define END_OF_CHAIN    (0)

// Identifies an individual byte within a block.
typedef int16_t offset_t;

// Used for storing the size of a file, or locating an individual byte
// within a file.
typedef uint32_t filesize_t;

enum block_kind {RAW, DIR, FAT};
typedef uint8_t block_kind_t;

struct dir_entry {
    blockno_t begin;
    block_kind_t kind;
    filesize_t size; // Size of file in bytes.
    bool in_use; // Whether we're actually storing a file here or not.
    char name[MAX_NAME];
};

struct raw_block {
    uint8_t data[BLOCK_SIZE];
};

struct dir_block {
    blockno_t self_addr;
    struct dir_entry entries[ENTRIES_PER_DIR_BLOCK];
};

struct fat_block {
    blockno_t tab[ENTRIES_PER_FAT_BLOCK];
};

enum {READ = 0, WRITE, APPEND};
typedef int8_t file_mode_t;

struct file_desc {
    file_mode_t mode;       // Read/write/append.
    blockno_t begin;        // The first block in the file.
    blockno_t block_no;     // The block to read or write from.
    offset_t offset;        // Where we are in that block.
    filesize_t pos;         // Where we are in the file total.
    filesize_t size;        // Total size of the file in bytes.
    blockno_t dir;          // The directory that owns the file.
    struct raw_block buf;   // We buffer one block at a time.
};

extern blockno_t fat[NUM_BLOCKS];


void write_raw(blockno_t, const struct raw_block *);
void write_fat(blockno_t, const struct fat_block *);
void write_dir(const struct dir_block *);

const struct raw_block *read_raw(blockno_t, struct raw_block *ret);
const struct fat_block *read_fat(blockno_t, struct fat_block *ret);
const struct dir_block *read_dir(blockno_t, struct dir_block *ret);

void sync_fat(void);

struct raw_block *fat_to_raw(
    const struct fat_block *restrict src,
    struct raw_block *restrict dst
);
struct fat_block *fat_from_raw(
    const struct raw_block *restrict src,
    struct fat_block *restrict dst
);

struct raw_block *dir_to_raw(
    const struct dir_block *src,
    struct raw_block *dst
);
struct dir_block *dir_from_raw(
    blockno_t self_addr,
    const struct raw_block *restrict src,
    struct dir_block *restrict dst
);

// Fills a directory block with a given block number for itself and no
// entries.
struct dir_block *blank_dir(blockno_t, struct dir_block *);

// Returns negative value if it cannot be parsed.
file_mode_t parse_mode(const char *mode);


#endif // FILESYS_INTERNAL_H
