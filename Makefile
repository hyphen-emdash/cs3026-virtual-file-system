DEBUG := -O0 -g3 -ggdb -fsanitize=address,undefined
RELEASE := -O2 -Os -DNDEBUG

CC := gcc
CFLAGS := -std=gnu11
CFLAGS += -Wall -Wextra -Wpedantic
CFLAGS += $(RELEASE)

all: shell

shell: main.o filesys.o filesys-internal.o util.o extra.o vec.o
	$(CC) $(CFLAGS) -o shell main.o filesys.o filesys-internal.o util.o extra.o vec.o

main.o: main.c filesys.h
	$(CC) $(CFLAGS) -c main.c

filesys.o: filesys.c filesys.h filesys-internal.h util.h
	$(CC) $(CFLAGS) -c filesys.c

filesys-internal.o: filesys-internal.c filesys.h filesys-internal.h util.h
	$(CC) $(CFLAGS) -c filesys-internal.c

util.o: util.c util.h
	$(CC) $(CFLAGS) -c util.c

extra.o: extra.c extra.h filesys.h filesys-internal.h
	$(CC) $(CFLAGS) -c extra.c

vec.o: vec.c vec.h
	$(CC) $(CFLAGS) -c vec.c

clean:
	rm *.o shell virtualdisk* trace*

edit:
	geany Makefile main.c filesys.h filesys.c filesys-internal.h \
	filesys-internal.c util.h util.c
