#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>

#include "filesys.h"
#include "filesys-internal.h"
#include "extra.h"
#include "util.h"

void test_D_grade(void);
void test_C_grade(void);
void test_B_grade(void);
void test_A_grade(void);

void test_copy(void);
void test_copy_in_out(void);
void test_chain(void);

void test_double_write(void);
void test_write_dir(void);
void test_many_files(void);

void test_extract(void);

int main(void) {
    test_A_grade();
    
    return 0;
}

void test_D_grade(void) {
    format();
    writedisk("virtualdiskD3_D1");
}

void test_C_grade(void) {
    format();
    
    // Write to file.
    MyFILE *testfile = myfopen("testfile.txt", "w");
    if (testfile == NULL) {
        fprintf(stderr, "Could not open file.\n");
        exit(1);
    }
    
    char text[4 * BLOCK_SIZE];
    for (size_t i = 0; i < sizeof(text); i++) {
        char str[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                     "abcdefghijklmnopqrstuvwxys";
        text[i] = str[i % 52];
    }
    
    for (size_t i = 0; i < sizeof(text); i++) {
        myfputc(text[i], testfile);
    }
    myfclose(testfile);
    
    // Read file back.
    testfile = myfopen("testfile.txt", "r");
    int c;
    while(c = myfgetc(testfile), c != EOF) {
        putchar(c);
    }
    putchar('\n');
    myfclose(testfile);
    
    writedisk("virtualdiskC3_C1");
}

void test_B_grade(void) {
    format();
    
    mymkdir("/myfirstdir/myseconddir/mythirddir/");
    
    char **ls = mylistdir("/myfirstdir//myfirstdir/myseconddir");
    if (ls == NULL) {
        printf("Could not read directory.\n");
        exit(1);
    }
    
    print_strlist(" * ", ls, "\n");
    
    free_strlist(ls);
    writedisk("virtualdiskB3_B1_a");
    
    
    MyFILE *testfile = myfopen("/myfirstdir/../myfirstdir/myseconddir/testfile.txt", "w");
    if (testfile == NULL) {
        fprintf(stderr, "Could not open file.\n");
        abort();
    }
    else {
        myfputc('X', testfile);
        myfclose(testfile);
        ls = mylistdir("/myfirstdir/./././myseconddir");
        if (ls == NULL) {
            fprintf(stderr, "Could not read directory.\n");
            abort();
        }
    }
    free_strlist(ls);
    writedisk("virtualdiskB3_B1_b");
}

void test_A_grade(void) {
    format();
    
    if (mymkdir("/firstdir/seconddir") == false) {
        fprintf(stderr, "Failed to create directories.\n");
        abort();
    }
    
    MyFILE *f = myfopen("firstdir/seconddir/testfile1.txt", "w");
    if (f == NULL) {
        fprintf(stderr, "Failed to open testfile1.txt\n");
        abort();
    }
    myfputs("Inside the first test file!", f);
    myfclose(f);
    
    char **ls = mylistdir("/firstdir/seconddir");
    if (ls == NULL) {
        fprintf(stderr, "Could not list files.\n");
        abort();
    }
    print_strlist(" * ", ls, "\n");
    putchar('\n');
    free_strlist(ls);
    
    if (mychdir("/firstdir/seconddir") == false) {
        fprintf(stderr, "Could not change directory.\n");
        abort();
    }
    
    ls = mylistdir(".");
    if (ls == NULL) {
        fprintf(stderr, "Could not list files.\n");
        abort();
    }
    print_strlist(" * ", ls, "\n");
    putchar('\n');
    free_strlist(ls);
    
    f = myfopen("testfile2.txt", "w");
    if (f == NULL) {
        fprintf(stderr, "Failed to open testfile2.txt\n");
        abort();
    }
    myfputs("Inside the second test file!", f);
    myfclose(f);
    
    if (mymkdir("thirddir") == false) {
        fprintf(stderr, "Could not open third directory.\n");
        abort();
    }
    
    f = myfopen("thirddir/testfile3.txt", "w");
    if (f == NULL) {
        fprintf(stderr, "Could not open testfile3.txt\n");
        abort();
    }
    myfputs("Inside the third test file!", f);
    myfclose(f);
    
    writedisk("virtualdiskA5_A1_a");
    
    if (myremove("testfile1.txt") == false) {
        fprintf(stderr, "Could not remove testfile1.txt\n");
        abort();
    }
    if (myremove("testfile2.txt") == false) {
        fprintf(stderr, "Could not remove testfile2.txt\n");
        abort();
    }
    
    writedisk("virtualdiskA5_A1_b");
    
    if (mychdir("thirddir") == false) {
        fprintf(stderr, "Could not move into thirddir/.\n");
        abort();
    }
    
    if (myremove("testfile3.txt") == false) {
        fprintf(stderr, "Could not remove testfile3.txt");
        abort();
    }
    
    writedisk("virtualdiskA5_A1_c");
    
    if (myrmdir(".") != false) {
        fprintf(stderr, "Was allowed to delete current directory.\n");
        abort();
    }
    
    if (mychdir("..") == false) {
        fprintf(stderr, "Could not move to parent directory.\n");
        abort();
    }
    
    if (myrmdir("thirddir") == false) {
        fprintf(stderr, "Could not remove thirddir/.\n");
        abort();
    }
    
    if (mychdir("/firstdir") == false) {
        fprintf(stderr, "Could not move into /firstdir.\n");
        abort();
    }
    
    myrmdir("seconddir");
    mychdir("/");
    myrmdir("firstdir");
    
    writedisk("virtualdiskA5_A1_d");
}

void test_copy(void) {
    format();
    
    MyFILE *source = myfopen("original.md", "w");
    
    const char *explanation =
    "# `mycopy()`\n"
    "\n"
    "We simply read from the source file one byte at a time, copying"
    "each byte to the destination file. We keep a counter of how many"
    "bytes have been read, and look out for write-errors.\n"
    "\n"
    "When we're done, we just return the count.\n";
    
    if (myfputs(explanation, source) < 0) {
        fprintf(stderr, "Had trouble writing to source for copying.\n");
        abort();
    }
    myfclose(source);
    
    source = myfopen("original.md", "r");
    MyFILE *destination = myfopen("copy.md", "w");
    
    assert(source != NULL);
    assert(destination != NULL);
    
    mycopy(source, destination);
    
    myfclose(destination);
    
    writedisk("virtualdisk_filecopy");
}

void test_copy_in_out(void) {
    format();
    
    MyFILE *internal = myfopen("original.txt", "w");
    for (int i = 0; i < 26; i++) {
        myfputc('A' + i, internal);
        myfputc('a' + i, internal);
    }
    myfclose(internal);
    internal = myfopen("original.txt", "r");
    
    FILE *external = fopen("copy.txt", "w");
    copy_out(internal, external);
    fclose(external);
    
    internal = myfopen("copy copy.txt", "w");
    external = fopen("copy.txt", "r");
    
    copy_in(external, internal);
    
    myfclose(internal);
    
    writedisk("virtualdisk_copy_in_out");
}

void test_chain(void) {
    format();
    
    mymkdir("0");
    mymkdir("1");
    MyFILE *f = myfopen("1/file.txt", "w");
    for (int i = 0; i < 5000; i++) {
        myfputc('G', f);
    }
    myfclose(f);
    
    print_fat();
}

void test_double_write(void) {
    format();
    
    MyFILE *testfile = myfopen("real.txt", "w");
    if (testfile == NULL) {
        fprintf(stderr, "Could not open file.\n");
        abort();
    }
    for (int i = 0; i < 6000; i++) {
        myfputc('A', testfile);
    }
    myfclose(testfile);
    writedisk("virtualdisk_double_write_a");

    testfile = myfopen("real.txt", "w");
    for (int i = 0; i < 50; i++) {
        myfputc('B', testfile);
    }
    myfclose(testfile);
    writedisk("virtualdisk_double_write_b");
}

void test_write_dir(void) {
    format();
    
    if (mymkdir("home") == false) {
        fprintf(stderr, "Could not open directory.\n");
        abort();
    }
    
    MyFILE *overwrite = myfopen("home", "w");
    if (overwrite == NULL) {
        printf("Was stopped from overwriting directory.\n");
    }
    else {
        printf("Was allowed to overwrite directory.\n");
    }
    
    myfclose(overwrite);
    writedisk("virtualdisk_write_dir");
}

void test_many_files(void) {
#define NUM_FILES (300)
    format();
    
    MyFILE *files[NUM_FILES];
    char names[NUM_FILES][MAX_NAME];
    
    // Create files.
    for (int i = 0; i < NUM_FILES; i++) {
        sprintf(names[i], "file-%d.txt", i);
        files[i] = myfopen(names[i], "w");
        if (files[i] == NULL) {
            fprintf(stderr, "Failed to open %s.\n", names[i]);
            abort();
        }
    }
    
    // Write to files.
    for (int i = 0; i < NUM_FILES; i++) {
        char message[100];
        sprintf(message, "I am in file no. %d.\n", i);
        if (myfputs(message, files[i]) == EOF) {
            fprintf(stderr, "Error writing to %s.\n", names[i]);
            abort();
        }
    }
    
    // Close files.
    for (int i = 0; i < NUM_FILES; i++) {
        myfclose(files[i]);
    }
    
    writedisk("virtualdisk_many_files");
    
#undef NUM_FILES
}

void test_extract(void) {
    const char *path = "/path/to//directory/./";
    //const char *path = "";
    //const char *path = "/";
    
    printf("path:\t\"%s\"\n", path);
    
    char **extracted = extract_path(path);
    
    print_strlist(" * ", extracted, "\n");
    
    free_strlist(extracted);
}
