#include "vec.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define ASSERT_VALID_VEC(v)\
    do {\
        assert((v).data != NULL);\
        assert((v).elt_size != 0);\
        assert((v).capacity != 0);\
    } while (0)

#define ASSERT_INVALID_VEC(v)\
    do {\
        assert((v).data == NULL);\
        assert((v).elt_size == 0);\
        assert((v).count == 0);\
        assert((v).capacity == 0);\
    } while (0)

#define ASSERT_VALID_SLICE(s)\
    do {\
        assert((s).src != NULL);\
        assert((s).begin < (s).src->count);\
        assert((s).begin + (s).count <= (s).src->count);\
    } while (0)

#define ASSERT_INVALID_SLICE(s)\
    do {\
        assert((s).src == NULL);\
        assert((s).begin == 0);\
        assert((s).count == 0);\
    } while (0)

static inline size_t next_pow2(size_t x) {
    size_t original = x;
    (void) original; // Unused variable warning when assertions are off.
    size_t ret = 2;
    
    while (x > 0) {
        x >>= 1;
        ret <<= 1;
    }
    assert(ret > original);
    return ret;
}

static inline size_t unsigned_mod(ptrdiff_t a, ptrdiff_t b) {
    a %= b;
    a += b;
    a %= b;
    return (size_t) a;
}


struct vec vec_new(size_t elt_size) {
    struct vec ret;
    ret.data = malloc(elt_size);
    if (ret.data == NULL) {
        ret.elt_size = 0;
        ret.count = 0;
        ret.capacity = 0;
    }
    else {
        ret.elt_size = elt_size;
        ret.count = 0;
        ret.capacity = 1;
    }
    return ret;
}

struct vec vec_from(size_t elt_size, size_t count, const void *data) {
    struct vec ret = vec_new(elt_size);
    if(ret.data != NULL && vec_resize(&ret, count)) {
        memcpy(ret.data, data, count * elt_size);
    }
    return ret;
}

struct vec vec_clone(struct vec src) {
    return vec_from(src.elt_size, src.count, src.data);
}

void *vec_freeable(struct vec *v) {
    if (v == NULL) {
        return NULL;
    }
    else if (v->data == NULL) {
        ASSERT_INVALID_VEC(*v);
        return NULL;
    }
    else {
        ASSERT_VALID_VEC(*v);
        void *ret = v->data;
        *v = (struct vec) {NULL, 0, 0, 0};
        return ret;
    }
}

void vec_clean(struct vec *v) {
    if (v == NULL) {
        return;
    }
    if (v->data == NULL) {
        ASSERT_INVALID_VEC(*v);
    }
    else {
        ASSERT_VALID_VEC(*v);
        free(v->data);
        *v = (struct vec) {NULL, 0, 0, 0};
    }
}

void *vec_at(struct vec v, size_t index) {
    ASSERT_VALID_VEC(v);
    
    if (index >= v.count) {
        return NULL;
    }
    return vec_unsafe_at(v, index);
}

void *vec_unsafe_at(struct vec v, size_t index) {
    return (unsigned char *) v.data + v.elt_size * index;
}

void *vec_cat(struct vec v, ptrdiff_t index) {
    ASSERT_VALID_VEC(v);
    
    if (v.count == 0) {
        return NULL;
    }
    
    size_t real_index = unsigned_mod(index, (ptrdiff_t) v.count);
    return vec_at(v, real_index);
}

bool vec_reserve(struct vec *v, size_t new_cap) {
    assert(v != NULL);
    
    ASSERT_VALID_VEC(*v);
    if (new_cap < v->count) {
        new_cap = v->count;
    }
    if (new_cap != v->capacity) {
        void *new_data = realloc(v->data, v->elt_size * new_cap);
        if (new_data == NULL) {
            return false;
        }
        v->data = new_data;
        v->capacity = new_cap;
    }
    return true;
}

bool vec_resize(struct vec *v, size_t new_count) {
    assert(v != NULL);
    ASSERT_VALID_VEC(*v);
    
    if (new_count <= v->count) {
        v->count = new_count;
        return true;
    }
    else {
        size_t new_cap = next_pow2(new_count); // Reduce allocations.
        bool success = vec_reserve(v, new_cap);
        if (success) {
            v->count = new_count;
        }
        return success;
    }
}


bool vec_push(struct vec *v, const void *elt) {
    assert(v != NULL);
    ASSERT_VALID_VEC(*v);
    
    if (!vec_resize(v, v->count+1)) {
        return false;
    }
    if (elt != NULL) {
        memcpy(
            (unsigned char *) v->data + v->elt_size * (v->count-1),
            elt,
            v->elt_size
        );
    }
    return true;
}

bool vec_pop(struct vec *v, void *out) {
    assert(v != NULL);
    
    if (v->count == 0) {
        return false;
    }
    
    vec_unsafe_pop(v, out);
    return true;
}

void vec_unsafe_pop(struct vec *v, void *out) {
    assert(v != NULL);
    
    vec_resize(v, v->count-1);
    if (out != NULL) {
        memcpy(
            out,
            (unsigned char *) v->data + v->elt_size * v->count,
            v->elt_size
        );
    }
}

struct vec_slice vec_as_slice(const struct vec *v) {
    assert(v != NULL);
    return (struct vec_slice) {v, 0, v->count};
}

void *vec_slice_at(struct vec_slice s, size_t index) {
    ASSERT_VALID_SLICE(s);
    if (index >= s.count) {
        return NULL;
    }
    return vec_slice_unsafe_at(s, index);
}

void *vec_slice_unsafe_at(struct vec_slice s, size_t index) {
    ASSERT_VALID_SLICE(s);
    return vec_at(*s.src, index + s.begin);
}


void *vec_slice_cat(struct vec_slice s, ptrdiff_t index) {
    ASSERT_VALID_SLICE(s);
    
    size_t real_index = unsigned_mod(index, (ptrdiff_t) s.count);
    return vec_slice_at(s, real_index);
}

