#ifndef EXTRA_H
#define EXTRA_H

#include "filesys.h"

// Some extra functionality.

// Writes a '\0'-terminated string to a file.
// (Doesn't write the '\0' itself.)
// Returns the number of characters written, negative on error.
ssize_t myfputs(const char *str, MyFILE *stream);

// For all the copy- functions below functions, `src` must be readable
// and `dst`
// must be writable.
// They take ownership of `src`, but not `dst`.

// Copies from virtual disk to virtual disk.
// Returns number of bytes copied, negative on error.
ssize_t mycopy(MyFILE *src, MyFILE *dst);

// Copies from real disk to virtual disk.
// Same return value as above.
ssize_t copy_in(FILE *src, MyFILE *dst);

// Copies from virtual disk to real disk.
// Same return value as above.
ssize_t copy_out(MyFILE *src, FILE *dst);

// Prints the chains of the fat.
void print_fat(void);


#endif // EXTRA_H
