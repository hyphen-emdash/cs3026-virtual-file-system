#include "util.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>
#include <unistd.h>

uint8_t *i8_to_le(int8_t x, uint8_t *bytes) {
    assert(bytes != NULL);
    
    bytes[0] = x;
    
    return bytes;
}

uint8_t *i16_to_le(int16_t x, uint8_t *bytes) {
    assert(bytes != NULL);
    
    bytes[0] = x & 0xff;
    bytes[1] = (x >> 8) & 0xff;
    
    return bytes;
}
uint8_t *i32_to_le(int32_t x, uint8_t *bytes) {
    assert(bytes != NULL);
    
    bytes[0] = x & 0xff;
    bytes[1] = (x >> 8) & 0xff;
    bytes[2] = (x >> 16) & 0xff;
    bytes[3] = (x >> 24) & 0xff;
    
    return bytes;
}

int8_t i8_from_le(const uint8_t *bytes, int8_t *ret) {
    assert(bytes != NULL);
    
    if (ret != NULL) {
        *ret = bytes[0];
    }
    
    return *ret;
}
int16_t i16_from_le(const uint8_t *bytes, int16_t *ret) {
    assert(bytes != NULL);
    
    int16_t val = bytes[0] | (bytes[1] << 8);
    if (ret != NULL) {
        *ret = val;
    }
    
    return val;
}
int32_t i32_from_le(const uint8_t *bytes, int32_t *ret) {
    assert(bytes != NULL);
    
    int32_t val = bytes[0]
                    | (bytes[1] << 8)
                    | (bytes[2] << 16)
                    | (bytes[3] << 24);
    if (ret != NULL) {
        *ret = val;
    }
    
    return val;
}

char **extract_path(const char *path) {
    struct vec parts = vec_new(sizeof(char *));
    if (parts.data == NULL) {
        goto err;
    }
    
    while (*path != '\0') {
        // Read the current substring.
        char buf[MAX_NAME];
        memset(buf, 0, MAX_NAME);
        size_t i;
        for (
            i = 0;
            i < MAX_NAME && path[i] != '\0' && path[i] != '/';
            i++
        ) {
            buf[i] = path[i];
        }
        if (i == MAX_NAME) {
            // One of the parts was too long.
            goto err;
        }
        path = path + i; // Move along to where we stopped reading.
        if (*path == '/') {
            // There's something more to read.
            path++;
        }
        
        // Add to the vector.
        char *new_str = strdup(buf);
        if (new_str == NULL) {
            goto err;
        }
        if (vec_push(&parts, &new_str) == false) {
            free(new_str);
            goto err;
        }
    } 
    
    // NULL-terminate before returning.
    char *terminator = NULL;
    if (vec_push(&parts, &terminator) == false) {
        goto err;
    }
    return vec_freeable(&parts);

err:
    free_strvec(&parts);
    return NULL;
}

char **strlist_end(char **strlist) {
    assert(strlist != NULL);
    
    while (strlist[0] != NULL) {
        strlist++;
    }
    return strlist;
}

void print_strlist(char *pre, char **list, char *post) {
    assert(list != NULL);
    
    for (size_t i = 0; list[i] != NULL; i++) {
        printf("%s%s%s", pre, list[i], post);
    }
}

void free_strlist(char **strlist) {
    if (strlist == NULL) {
        return;
    }
    
    for (size_t i = 0; strlist[i] != NULL; i++) {
        free(strlist[i]);
    }
    
    free(strlist);
}

void free_strvec(struct vec *v) {
    if (v == NULL) {
        return;
    }
    
    for (size_t i = 0; i < v->count; i++) {
        free(*(char **) vec_at(*v, i));
    }
    
    vec_clean(v);
}
