#include "filesys.h"
#include "filesys-internal.h"

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "util.h"

struct packed_dir_entry {
    uint8_t begin[sizeof(blockno_t)];
    uint8_t kind[sizeof(block_kind_t)];
    uint8_t size[sizeof(filesize_t)];
    uint8_t in_use[1];
    uint8_t name[MAX_NAME];
};
STATIC_ASSERT(
    sizeof(struct packed_dir_entry) <= sizeof(struct dir_entry),
    dir_entry_unpackable
);

blockno_t fat[NUM_BLOCKS];

static struct raw_block virtual_disk[NUM_BLOCKS];

struct packed_dir_entry *pack_dir_entry(
    const struct dir_entry *src,
    struct packed_dir_entry *dst
);
struct dir_entry *unpack_dir_entry(
    const struct packed_dir_entry *src,
    struct dir_entry *dst
);

void writedisk(const char *filename) {
    assert(filename != NULL);
    
    FILE *f = fopen(filename, "w"); // Slightly less virtual disk.
    
    if (f == NULL) {
        fprintf(stderr, "Could not open ''disk'' to write virtual disk.\n");
        return;
    }
    
    if (fwrite(&virtual_disk, sizeof(virtual_disk), 1, f) < 1) {
        fprintf(stderr, "Error writing virtual disk.");
    }
    
    fclose(f);
}

void write_raw(blockno_t addr, const struct raw_block *raw) {
    assert(0 <= addr);
    assert(addr < NUM_BLOCKS);
    assert(raw != NULL);
    
    virtual_disk[addr] = *raw;
}

void write_fat(blockno_t addr, const struct fat_block *fat) {
    assert(fat != NULL);
    
    struct raw_block raw;
    fat_to_raw(fat, &raw);
    write_raw(addr, &raw);
}

void write_dir(const struct dir_block *dir) {
    assert(dir != NULL);
    
    struct raw_block raw;
    dir_to_raw(dir, &raw);
    write_raw(dir->self_addr, &raw);
}

const struct raw_block *read_raw(blockno_t addr, struct raw_block *ret) {
    assert(0 <= addr);
    assert(addr < NUM_BLOCKS);
    assert(ret != NULL);
    
    extern struct raw_block virtual_disk[NUM_BLOCKS];
    *ret = virtual_disk[addr];
    
    return ret;
}

const struct fat_block *read_fat(blockno_t addr, struct fat_block *ret) {
    assert(ret != NULL);
    
    struct raw_block raw;
    read_raw(addr, &raw);
    fat_from_raw(&raw, ret);
    
    return ret;
}
const struct dir_block *read_dir(blockno_t addr, struct dir_block *ret) {
    assert(ret != NULL);
    
    struct raw_block raw;
    read_raw(addr, &raw);
    dir_from_raw(addr, &raw, ret);
    
    return ret;
}

void sync_fat(void) {
    extern struct raw_block virtual_disk[NUM_BLOCKS];
    extern blockno_t fat[NUM_BLOCKS];
    
    struct fat_block temp;
    
    for (blockno_t block_i = 0; (unsigned) block_i < FAT_BLOCKS; block_i++) {
        for (
            size_t i = 0;
            i < ENTRIES_PER_FAT_BLOCK && block_i*ENTRIES_PER_FAT_BLOCK + i < NUM_BLOCKS;
            i++
        ) {
            temp.tab[i] = fat[block_i * ENTRIES_PER_FAT_BLOCK + i];
        }
        write_fat(FAT_BEGIN + block_i, &temp);
    }
}


struct raw_block *fat_to_raw(
    const struct fat_block *restrict src,
    struct raw_block *restrict dst
) {
    assert(src != NULL);
    assert(dst != NULL);
    
    for (size_t i = 0; i < ENTRIES_PER_FAT_BLOCK; i++) {
        i16_to_le(src->tab[i], dst->data + i*2);
    }
    
    return dst;
}


struct fat_block *fat_from_raw(
    const struct raw_block *restrict src,
    struct fat_block *restrict dst
) {
    assert(src != NULL);
    assert(dst != NULL);
    
    for (size_t i = 0; i < ENTRIES_PER_FAT_BLOCK; i++) {
        i16_from_le(src->data + i * sizeof(blockno_t), dst->tab + i);
    }
    
    return dst;
}

struct raw_block *dir_to_raw(
    const struct dir_block *src,
    struct raw_block *dst
) {
    assert(src != NULL);
    assert(dst != NULL);
    
    memset(dst, 0, sizeof(struct raw_block));
    
    for (size_t i = 0; i < ENTRIES_PER_DIR_BLOCK; i++) {
        struct packed_dir_entry packed;
        pack_dir_entry(src->entries + i, &packed);
        
        memcpy(
            dst->data + i * sizeof(struct packed_dir_entry),
            &packed,
            sizeof(struct packed_dir_entry)
        );
    }
    
    return dst;
}
struct dir_block *dir_from_raw(
    blockno_t self_addr,
    const struct raw_block *restrict src,
    struct dir_block *restrict dst
) {
    assert(src != NULL);
    assert(dst != NULL);
    
    dst->self_addr = self_addr;
    
    for (size_t i = 0; i < ENTRIES_PER_DIR_BLOCK; i++) {
        struct dir_entry unpacked;
        unpack_dir_entry(
            ((struct packed_dir_entry *) src->data) + i,
            &unpacked
        );
        dst->entries[i] = unpacked;
    }
    
    return dst;
}

struct dir_block *blank_dir(blockno_t addr, struct dir_block *ret) {
    assert(ret != NULL);
    
    memset(ret, 0, sizeof(struct dir_block));
    
    ret->self_addr = addr;
    for (size_t i = 0; i < ENTRIES_PER_DIR_BLOCK; i++) {
        ret->entries[i] = (struct dir_entry) {
            .begin = 0,
            .kind = RAW,
            .size = 0,
            .in_use = false,
        };
        memset(ret->entries[i].name, 0, MAX_NAME);
    }
    
    return ret;
}

file_mode_t parse_mode(const char *mode) {
    if (strcmp(mode, "r") == 0) {
        return READ;
    }
    if (strcmp(mode, "w") == 0) {
        return WRITE;
    }
    if (strcmp(mode, "a") == 0) {
        return APPEND;
    }
    return -1;
}


struct packed_dir_entry *pack_dir_entry(
    const struct dir_entry *src,
    struct packed_dir_entry *dst
) {
    assert(src != NULL);
    assert(dst != NULL);
    
    int_to_le(src->begin, dst->begin);
    int_to_le(src->kind, dst->kind);
    int_to_le(src->size, dst->size);
    int_to_le(src->in_use, dst->in_use);
    memset(dst->name, 0, MAX_NAME);
    strncpy((char *) dst->name, src->name, MAX_NAME - 1);
    
    return dst;
}

struct dir_entry *unpack_dir_entry(
    const struct packed_dir_entry *src,
    struct dir_entry *dst
) {
    assert(src != NULL);
    assert(dst != NULL);
    
    int_from_le(src->begin, &dst->begin);
    int_from_le(src->kind, (int8_t *) &dst->kind);
    int_from_le(src->size, (int32_t *) &dst->size);
    int_from_le(src->in_use, (int8_t *) &dst->in_use);
    memset(dst->name, 0, MAX_NAME);
    strncpy(dst->name, (char *) src->name, MAX_NAME - 1);
    
    return dst;
}
